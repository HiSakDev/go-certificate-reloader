package certreload

import "bytes"

func contains(s [][]byte, b []byte) bool {
	for _, v := range s {
		if bytes.Equal(v, b) {
			return true
		}
	}
	return false
}
